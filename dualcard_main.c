#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <usb.h>
#include "dlldefine.h"

int main(int argc, char *argv[])
{
	int usbDevice,dp680;
	int bDataExchange = -1;
	int rlen,i,slen,timeout;
	unsigned char sender[64+1],resp[64+1];

	usbDevice = dualiUSBOpenDevice(0x0650); //PID=0635, 
	if( usbDevice < 0 )
	{	
		printf("USBOpenDevice  function return NULL!\n %d",usbDevice);
		return -1;
	}
	for(dp680=0;dp680 < usbDevice;dp680++){
		timeout = 1000;
		slen = 0;
		sender[slen++] = _DE_VERSION;
		bDataExchange = DE_Polling(USBFS1+dp680, slen, sender, &rlen, resp, timeout);
		if(bDataExchange >= 0)
		{
			if(bDataExchange == 0x00) printf("DE_GetVersion : OK\n");
			else printf("DE_GetVersion : fail: %02X\n",bDataExchange);

			if(rlen){
				printf("hbuf==> ");
				for(i=0;i<rlen;i++){
					printf("%02X ",resp[i]);
				}
			}
			printf("\n");
		}else{
			printf("\nUSB Control Error\n");
		}


		timeout = 5000;
		slen = 0;
		sender[slen++] = _DE_FIND_CARD;
		sender[slen++] = 0;
		sender[slen++] = 0;
		sender[slen++] = 1;
		sender[slen++] = 0;

		bDataExchange = DE_Polling(USBFS1+dp680, slen, sender, &rlen, resp, timeout);
		if(bDataExchange >= 0)
		{
			if(bDataExchange == 0x00) printf("0DE_FindCard : OK\n");
			else printf("0DE_FindCard : fail: %02X\n",bDataExchange);

			if(rlen){
				printf("hbuf==> ");
				for(i=0;i<rlen;i++){
					printf("%02X ",resp[i]);
				}
				printf("\n");
			}
		}

	}

	dualiUSBCloseDevice();
	return 0;
}

