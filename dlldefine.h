///////////////////////////////////////////////////////////
//Define
///////////////////////////////////////////////////////////
//return value
#define DE_OK					0
#define DE_NO_TAG_ERROR    			2 //(0x02)
#define DE_CRC_ERROR       			3 //(0x03)
#define DE_EMPTY				4 //(0x04)(NO IC CARD ERROR)	
#define DE_AUTHENTICATION_ERROR     		5 //(0x05)
#define DE_NO_POWER				5 //(0x05)
#define DE_PARITY_ERROR    			6 //(0x06)
#define DE_CODE_ERROR      			7 //(0x07)
#define DE_SERIAL_NUMBER_ERROR      		8 //(0x08)
#define DE_KEY_ERROR       			9 //(0x09)
#define DE_NOT_AUTHENTICATION_ERROR    		10 //(0x0A)
#define DE_BIT_COUNT_ERROR   			11 //(0x0B)	
#define DE_BYTE_COUNT_ERROR 			12 //(0x0C)		
#define DE_TRANSFER_ERROR      			14 //(0x0E)	
#define DE_WRITE_ERROR       			15 //(0x0F)	
#define DE_INCREMENT_ERROR         		16 //(0x10)	
#define DE_DECREMENT_ERROR         		17 //(0x11)	
#define DE_READ_ERROR         			18 //(0x12)	
#define DE_OVERFLOW_ERROR     			19 //(0x13)	
#define DE_POLLING_ERROR         		20 //(0x14)	
#define DE_FRAMING_ERROR         		21 //(0x15)	
#define DE_ACCESS_ERROR        			22 //(0x16)	
#define DE_UNKNOWN_COMMAND_ERROR		23 //(0x17)	
#define DE_ANTICOLLISION_ERROR         		24 //(0x18)	
#define DE_INITIALIZATION_ERROR			25 //(0x19)	
#define DE_INTERFACE_ERROR   		 	26 //(0x1A)	
#define DE_ACCESS_TIMEOUT_ERROR			27 //(0x1B)	
#define DE_NO_BITWISE_ANTICOLLISION_ERROR	28 //(0x1C)
#define DE_FILE_ERROR				29 //(0x1D)
#define DE_INVAILD_BLOCK_ERROR			32 //(0x20)
#define DE_ACK_COUNT_ERROR			33 //(0x21)
#define DE_NACK_DESELECT_ERROR			34 //(0x22)
#define DE_NACK_COUNT_ERROR			35 //(0x23)
#define DE_SAME_FRAME_COUNT_ERROR		36 //(0x24)
#define DE_RCV_BUFFER_TOO_SMALL_ERROR		49 //(0x31)
#define DE_RCV_BUFFER_OVERFLOW_ERROR		50 //(0x32)
#define DE_RF_ERROR				51 //(0x33)
#define DE_PROTOCOL_ERROR			52 //(0x34)
#define DE_USER_BUFFER_FULL_ERROR		53 //(0x35)
#define DE_BUADRATE_NOT_SUPPORTED		54 //(0x36)
#define DE_INVAILD_FORMAT_ERROR			55 //(0x37)
#define DE_LRC_ERROR				56 //(0x38)
#define DE_FRAMERR				57 //(0x39)
#define DE_WRONG_PARAMETER_VALUE		60 //(0x3C)
#define DE_INVAILD_PARAMETER_ERROR		61 //(0x3D)
#define DE_UNSUPPORTED_PARAMETER		62 //(0x3E)
#define DE_UNSUPPORTED_COMMAND			63 //(0x3F)
#define DE_INTERFACE_NOT_ENABLED		64 //(0x40)
#define DE_ACK_SUPPOSED				65 //(0x41)
#define DE_NACK_RECEVIED			66 //(0x42)
#define DE_BLOCKNR_NOT_EQUAL			67 //(0x43)
#define DE_TARGET_SET_TOX			68 //(0x44)
#define DE_TARGET_RESET_TOX			69 //(0x45)
#define DE_TARGET_DESELECTED			70 //(0x46)
#define DE_TARGET_RELEASED			71 //(0x47)
#define DE_ID_ALREADY_IN_USE            	72 //(0x48)
#define DE_INSTANCE_ALREADY_IN_USE		73 //(0x49)
#define DE_ID_NOT_IN_USE			74 //(0x4A)
#define DE_NO_ID_AVAILABLE              	75 //(0x4B)
#define DE_OTHER_ERROR				76 //(0x4C)
#define DE_INVALID_READER_STATE			77 //(0x4D)
#define DE_MI_JOINER_TEMP_ERROR			78 //(0x4C)
#define DE_NOTYET_IMPLEMENTED			100//(0x64)
#define DE_FIFO_ERROR				109//(0x6D)
#define DE_WRONG_SELECT_COUNT			114//(0x72)
#define DE_WRONG_VALUE				123//(0x7B)
#define DE_VALERR				124//(0x7C)
#define DE_RE_INIT				126//(0x7E)
#define DE_NO_INIT				127//(0x7F)
#define APP_INVALID_PORT			1000
#define APP_STX_ERROR				1001
#define APP_INVALID_LENGTH_ERROR		1002
#define APP_TIMEOUT_ERROR			1003
#define APP_CRC_ERROR				1004
#define APP_LRC_ERROR				1005
#define APP_RW_ERROR				1006
#define APP_ETX_ERROR				1007
#define APP_USB_WRITE_ERROR			1008
#define APP_USB_READ_ERROR			1009
#define APP_INVALID_SENDDATA_LEN		1010
#define APP_INVALID_SENDBUF_SIZE		1011
#define APP_TOO_SMALL_RECVBUF			1012
#define APP_SENDBUF_OVERFLOW			1013
#define APP_MODEM_ERROR_START			1024
//////////////////////////////////////
//Device control command
//////////////////////////////////////
#define _DE_RFON					0x10
#define _DE_RFOFF					0x11
#define _DE_RESET					0x12
#define _DE_BUZZER					0x13
#define _DE_DEV_CHANGE				0x15
#define _DE_VERSION					0x16
#define _DE_TRXSPEED				0x1A
#define _DE_RF_WTX					0x1B
#define _DE_CONTACT_WTX				0x1C
#define _DE_FLASH					0x1F
#define _DE_CONTACT_ANTI_TEARING	0x18
#define _DE_RF_ANTI_TEARING			0x19
#define _DE_RF_RESET				0x20

//////////////////////////////////////
//command
//////////////////////////////////////

//////////////////////////////////////
//TYPE C
//////////////////////////////////////
#define _DEC_TRANSPARENT			0x50
#define _DEC_POLLING_NOENC			0x51
#define _DEC_READ_NOENC				0x52
#define _DEC_WRITE_NOENC			0x53
//////////////////////////////////////
//TYPE B
//////////////////////////////////////
#define _DEB_TRANSPARENT			0x60
#define _DEB_TRANSPARENT2			0x6E
#define _DEB_BFRAMING				0x6F
//////////////////////////////////////
//TYPE A
//////////////////////////////////////
#define _DEA_RESET					0x20
#define _DEA_IDLE_REQ				0x21
#define _DEA_WAKEUP_REQ				0x22
#define _DEA_ANTICOLL				0x23
#define _DEA_SELECT					0x24
#define _DEA_AUTH					0x25
#define _DEA_HALT					0x26
#define _DEA_READ					0x27
#define _DEA_WRITE					0x28
#define _DEA_INCREMENT				0x29
#define _DEA_DECREMENT				0x2A
#define _DEA_INC_TRANS				0x2B
#define _DEA_DEC_TRANS				0x2C
#define _DEA_RESTORE				0x2D
#define _DEA_TRANSFER				0x2E
#define _DEA_LOADKEY				0x2F
#define _DEA_AUTHKEY				0x30
#define _DEA_REQ_ANTI_AUTH			0x31
#define _DEA_REQ_ANTI_AUTHKEY		0x32
#define _DEA_INC_TRANS2				0x33
#define _DEA_DEC_TRANS2				0x34
#define _DEA_REQ_ANTI_AUTH_RD		0x35
#define _DEA_REQ_ANTI_AUTHKEY_RD    0x36
#define _DEA_REQ_ANTI_AUTH_WR		0x37
#define _DEA_REQ_ANTI_AUTHKEY_WR	0x38
#define _DEA_REQ_ANTI_SEL			0x39
#define _DEA_UWRITE  				0x3B
#define _DEA_ANTI_SEL_LEVEL			0x3D
#define _DEA_ANTICOLL_LEVEL			0x3E
#define _DEA_SELECT_LEVEL			0x3F
#define _DEA_DEVINFO				0x40
#define _DEA_TRANSPARENT			0x41
#define _DEA_TRANSPARENT2			0x47
#define _DEA_BITMODE				0xA1
#define _DEA_BITMODEANTI			0xA2
#define _DEA_BITMODE2				0xA4
///////////////////////////////////////
//TYPE A/B Common
///////////////////////////////////////
#define _DE_FIND_CARD				0x4C
#define _DE_APDU					0x61
#define _DEAB_RW_WRITE				0x7A
#define _DEAB_RW_READ				0x7B


///////////////////////////////////////
//15693
///////////////////////////////////////
#define _DED_Inventory				0x70
#define _DED_Select					0x71
#define _DED_Read					0x72
#define _DED_Write					0x73
#define _DED_Transparent			0x74
#define _DED_Eof					0x78


///////////////////////////////////////
//PCSC Autopolling
///////////////////////////////////////
#define _PCSC_CONNECT           0x80
#define _PCSC_POLLING_SET       0x81
#define _PCSC_POLLING_SET2      0x82
///////////////////////////////////////

///////////////////////////////////////
//Contact card command
///////////////////////////////////////
#define _DE_CARD_PON				0xC0
#define _DE_CARD_CASE1				0xC1
#define _DE_CARD_CASE2				0xC2
#define _DE_CARD_CASE3				0xC3
#define _DE_CARD_CASE4				0xC4
#define _DE_CARD_POFF				0xC5
#define _DE_CARD_T1BYPASS			0xC7
#define _DE_CARD_SPEED				0xC8
#define _DE_CARD_APDU				0xC9
#define _DE_CARD_PARITY_ERROR_TEST  0xCA

////////////////////////////////////////
//NFC
////////////////////////////////////////
#define _NFC_INIT								0x90
#define _NFC_INITIATOR_COMM						0x91
#define _NFC_TARGET_COMM						0x92
#define _NFC_CARD_MODE  						0x93
#define _NFC_ISTART  							0x94
#define _NFC_TSTART  							0x95
#define _NFC_SEND_DATAI  						0x96
#define _NFC_SEND_DATAT  						0x97
#define _NFC_STOP  								0x98
#define _NFC_GET_TARGET							0x99
#define _NFC_SET_TARGET							0x9A
#define _NFC_TARGET_STATE						0x9B
#define _NFC_TAG_COMMAND						0x9E
#define _EXCHANGE_APDU							0xF0
///////////////////////////////////////
//NFC command
///////////////////////////////////////
#define _LLC_ACTIVATE				0xBE
#define _LLC_MACDEACTIVETION		0xBF
#define _LLC_CONNECTREQUEST			0xB0
#define _LLC_CONNECTRESPONSE		0xB1
#define _LLC_DISCONNECTREQUEST		0xB3
#define _LLC_SENDRECIEVEREADY		0xB7
#define _LLC_SENDSYMMETRY			0xB4
#define _LLC_SENDINFORMATION		0xB2
#define _LLC_SENDTEXT				0xBC
#define _LLC_RECEIVETEXT			0xBD
#define _LLC_TAG_EMUL				0xBA

#define _DE_FIND_TAG				0x9C
#define _DE_PICC_CONNECT			0x83

#define USBFS1	2000

///////////////////////////////////////
//DesFire Batch command
///////////////////////////////////////
#define _DE_DESFIRE_BATCH			0x3A

int dualiUSBOpenDevice(unsigned short pid);
int dualiUSBCloseDevice(void);

int De_Get_ErrMsg(int errcode,unsigned char* retmsg);
void DE_SETDBG(int dbgflag);

//========================================================
//	Common functions
//========================================================
extern int DE_InitPort(int virtualPort, int nBaud);
extern int DE_ClosePort(int virtualPort);

//RF function for all terminals
extern int DE_RFOn(int virtualPort, int* outlen, unsigned char* lpRes);
extern int DE_RFOff(int virtualPort, int* outlen, unsigned char* lpRes);
extern int DE_RFReset(int virtualPort, int* outlen, unsigned char* lpRes);
extern int DE_BuzzerOn(int virtualPort);
extern int DE_BuzzerOff(int virtualPort);
extern int DE_GetVersion(int virtualPort, int* outlen, unsigned char* lpRes);
extern int DE_Polling(int virtualPort, int datalen, unsigned char* data, int* outlen, unsigned char* lpRes, int xtimeout);

extern int DE_RF_Sta(void);

extern int DEB_Transparent(int virtualPort, unsigned char datalen, unsigned char* data, unsigned char TOUT, int* outlen, unsigned char* lpRes);
extern int DEB_TransparentCRC(int virtualPort, unsigned char datalen, unsigned char* data, unsigned char* crc,
		unsigned char TOUT, int* outlen, unsigned char* lpRes);
extern int DEB_BFRAMING(int virtualPort, unsigned char Fvalue);
extern int DEA_Idle_Req(int virtualPort, int* outlen, unsigned char* lpRes);
extern int DEA_Wakeup_Req(int virtualPort, int* outlen, unsigned char* lpRes);
extern int DEA_Anticoll(int virtualPort, int* outlen, unsigned char* lpRes);
extern int DEA_Select(int virtualPort, unsigned char* uid, int* outlen, unsigned char* lpRes);
extern int DEA_Auth(int virtualPort, unsigned char mode, unsigned char keyno, unsigned char blockno);
extern int DEA_Halt(int virtualPort);
extern int DEA_Read(int virtualPort, unsigned char blockno, int* outlen, unsigned char* lpRes);
extern int DEA_Write(int virtualPort, unsigned char blockno, int datalen, unsigned char* data);
extern int DEA_Inc_Transfer(int virtualPort, unsigned char blockno, unsigned char* value, unsigned char trblockno);
extern int DEA_Decrement(int virtualPort, unsigned char blockno, unsigned char* value);
extern int DEA_Dec_Transfer(int virtualPort, unsigned char blockno, unsigned char* value, unsigned char trblockno);
extern int DEA_Restore(int virtualPort, unsigned char blockno);
extern int DEA_Transfer(int virtualPort, unsigned char blockno);
extern int DEA_Loadkey(int virtualPort, unsigned char mode, unsigned char keyno, unsigned char* keydata);
extern int DEA_Authkey(int virtualPort, unsigned char mode, unsigned char* keydata, unsigned char blockno);

extern int DEA_Req_Auth(int virtualPort, unsigned char requestmode, unsigned char authmode,
		unsigned char keyno, unsigned char blockno, int* outlen, unsigned char* lpRes);
extern int DEA_Req_Authkey(int virtualPort, unsigned char requestmode, unsigned char authmode,
		unsigned char blockno, unsigned char* keydata, int* outlen, unsigned char* lpRes);
extern int DEA_Inc_Transfer2(int virtualPort, unsigned char blockno, unsigned char* value, unsigned char trblockno);
extern int DEA_Dec_Transfer2(int virtualPort, unsigned char blockno, unsigned char* value, unsigned char trblockno);

extern int DEA_Req_AuthRead(int virtualPort, unsigned char requestmode, unsigned char authmode,
		unsigned char keyno, unsigned char blockno, int* outlen, unsigned char* lpRes);
extern int DEA_Req_AuthkeyRead(int virtualPort, unsigned char requestmode, unsigned char authmode,
		unsigned char blockno, unsigned char* keydata, int* outlen, unsigned char* lpRes);
extern int DEA_Req_AuthWrite(int virtualPort, unsigned char requestmode, unsigned char authmode,
		unsigned char keyno, unsigned char blockno,  unsigned char* data, int* outlen, unsigned char* lpRes);
extern int DEA_Req_AuthkeyWrite(int virtualPort, unsigned char requestmode, unsigned char authmode,
		unsigned char blockno,  unsigned char* keydata, unsigned char* data, int* outlen, unsigned char* lpRes);
extern int DEA_Req_Select(int virtualPort, unsigned char requestmode, int* outlen, unsigned char* lpRes);

extern int DEA_UltraM_Write(int virtualPort, unsigned char address, unsigned char* data);
extern int DEA_AntiSelLevel(int virtualPort, int* outlen, unsigned char* lpRes);
extern int DEA_AnticollLevel(int virtualPort, unsigned char cmd, unsigned char bitcnt, unsigned char* uid ,int* outlen, unsigned char* lpRes);
extern int DEA_SelectLevel(int virtualPort, unsigned char cmd, unsigned char* uid , int* outlen, unsigned char* lpRes);
extern int DEA_DeviceInfo(int virtualPort, int* outlen, unsigned char* lpRes);
extern int DEA_Transparent(int virtualPort, unsigned char datalen, unsigned char* data, unsigned char TOUT, int* outlen, unsigned char* lpRes);
extern int DEA_TransparentCRC(int virtualPort, unsigned char datalen, unsigned char* data, unsigned char* crc,
		unsigned char TOUT, int* outlen, unsigned char* lpRes);
extern int DEA_BitMode(int virtualPort, unsigned char datalen, unsigned char TxByteNo, unsigned char TxBitNo,
		unsigned char* data, int* outlen, unsigned char* lpRes);
extern int DEA_BitModeAnti(int virtualPort, unsigned char datalen, unsigned char TxByteNo, unsigned char TxBitNo,
		unsigned char* data, int* outlen, unsigned char* lpRes);
extern int DEA_BitMode2(int virtualPort, unsigned char datalen, unsigned char TxByteNo, unsigned char TxBitNo,
		unsigned char* data, int* outlen, unsigned char* lpRes);

extern int DE_APDU(int virtualPort, int datalen, unsigned char* data, int* outlen, unsigned char* lpRes);
extern int DE_FindCard(int virtualPort, unsigned char baud, unsigned char cid,
		unsigned char nad, unsigned char option, int* outlen, unsigned char* lpRes);
extern int DEAB_RwWrite(int virtualPort, unsigned char Data);
extern int DEAB_RwRead(int virtualPort, int* outlen, unsigned char* lpRes);

extern int DED_Inventory(int virtualPort, unsigned char Flag, int* outlen, unsigned char* lpRes);
extern int DED_Select(int virtualPort, int* outlen, unsigned char* lpRes);
extern int DED_Read(int virtualPort, unsigned char blockno, int* outlen, unsigned char* lpRes);
extern int DED_Write(int virtualPort, unsigned char blockno, unsigned char* data, int* outlen, unsigned char* lpRes);
extern int DED_Transparent(int virtualPort, int datalen, unsigned char* data, int* outlen, unsigned char* lpRes);

//FeliCa function
extern int DEC_Transparent(int virtualPort, int datalen, unsigned char* data, int* outlen,
		unsigned char* lpRes, unsigned char tout);

//15693 functions
extern int DED_Eof(int virtualPort, int* outlen, unsigned char* lpRes);

// nfc
extern int DE_NFC_TAG_CMD(int virtualPort, unsigned char TagType,unsigned char TagCMD, int optdatalen, unsigned char* optdata, int* outlen, unsigned char* lpRes);
extern void DE_GetDLLVersion(int* outlen, unsigned char* lpRes);


//FeliCa functions
//for DE-620, DE-ABCM only
extern int DEC_Polling_NoENC(int virtualPort, unsigned char* systemcode, unsigned char requestsyscode,
		unsigned char timeslot, int* outlen, unsigned char* lpRes, unsigned char tout);
extern int DEC_Read_NoENC(int virtualPort, unsigned char* IDm, unsigned char* servicecode,
		unsigned char block, int* outlen, unsigned char* lpRes, unsigned char tout);
extern int DEC_Write_NoENC(int virtualPort, unsigned char* IDm, unsigned char* servicecode,
		unsigned char block, unsigned char* blockdata, int* outlen, unsigned char* lpRes, unsigned char tout);


