#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


#include "dlldefine.h"
#include <libusb.h>

#define USB_VENDOR_ID	    0x1db2      
//#define USB_PRODUCT_ID	    0x0635      /* USB product ID used by the device */

unsigned char outendpoint=-1,inendpoint=-1;

static libusb_device_handle *dualiUsbDevice0;
static libusb_device_handle *dualiUsbDevice1;

#define u8	unsigned char
#define u16	unsigned short
#define u32	unsigned int
#define u64	unsigned long
typedef struct
{
	int wlen; // 쓸 데이터 길이
	int rlen; // 읽을 데이터 길이
	unsigned char cmd; // 커맨드
	unsigned char buf[4096]; // 데이터 버퍼
}ioctl_arg;

int debugflag = 0;


	#define LOGD(...)  if(debugflag >0) {printf("\nDEBUG:");	printf(__VA_ARGS__); printf("\n"); }
	#define LOGE(...)  printf("\nERROR:");	printf(__VA_ARGS__); printf("\n")
void DE_SETDBG(int dbgflag)
{
	debugflag = dbgflag;
}
int De_Get_ErrMsg(int errcode,unsigned char* retmsg)
{
	char errmsg[64];
	
//	memset(retmsg,0x00,sizeof(retmsg));

	switch(errcode)
	{
	case 0:
		sprintf(errmsg,"OK");
		break;
	case 2:
		sprintf(errmsg,"NO_TAG_ERROR");
		break;
	case 3:
		sprintf(errmsg,"CRC_ERROR");
		break;
	case 4:
		sprintf(errmsg,"EMPTY");
		break;
	case 5:
		sprintf(errmsg,"AUTHENTICATION_ERROR or DE_NO_POWER");
		break;	
	case 6:
		sprintf(errmsg,"PARITY_ERROR");
		break;
	case 7:
		sprintf(errmsg,"CODE_ERROR");
		break;
	case 8:
		sprintf(errmsg,"SERIAL_NUMBER ERROR");
		break;
	case 9:
		sprintf(errmsg,"KEY_ERROR");
		break;
	case 10:
		sprintf(errmsg,"NOT_AUTHENTICATION ERROR");
		break;
	case 11:
		sprintf(errmsg,"BIT_COUNT_ERROR");
		break;
	case 12:
		sprintf(errmsg,"BYTE_COUNT_ERROR");
		break;
	case 14:
		sprintf(errmsg,"TRANSFER_ERROR");
		break;
	case 15:
		sprintf(errmsg,"WRITE_ERROR");
		break;
	case 16:
		sprintf(errmsg,"INCREMENT_ERROR");
		break;
	case 17:
		sprintf(errmsg,"DECREMENT_ERROR");
		break;
	case 18:
		sprintf(errmsg,"READ_ERROR");
		break;
	case 19:
		sprintf(errmsg,"OVERFLOW_ERROR");
		break;
	case 20:
		sprintf(errmsg,"POLLING_ERROR");
		break;
	case 21:
		sprintf(errmsg,"FRAMING_ERROR");
		break;
	case 22:
		sprintf(errmsg,"ACCESS_ERROR");
		break;
	case 23:
		sprintf(errmsg,"UNKNOWN_COMMAND_ERROR");
		break;
	case 24:
		sprintf(errmsg,"ANTICOLLISION_ERROR");
		break;
	case 25:
		sprintf(errmsg,"INITIALIZATION_ERROR");
		break;
	case 26:
		sprintf(errmsg,"INTERFACE_ERROR");
		break;
	case 27:
		sprintf(errmsg,"ACCESS_TIMEOUT_ERROR");
		break;
	case 28:
		sprintf(errmsg,"NO_BITWISE_ANTICOLLISION_ERROR");
		break;
	case 29:
		sprintf(errmsg,"FILE_ERROR");
		break;
	case 32:
		sprintf(errmsg,"INVAILD_BLOCK_ERROR");
		break;
	case 33:
		sprintf(errmsg,"ACK_COUNT_ERROR");
		break;
	case 34:
		sprintf(errmsg,"NACK_DESELECT_ERROR");
		break;
	case 35:
		sprintf(errmsg,"NACK_COUNT_ERROR");
		break;
	case 36:
		sprintf(errmsg,"SAME_FRAME_COUNT_ERROR");
		break;
	case 49:
		sprintf(errmsg,"RCV_BUFFER_TOO_SMALL_ERROR");
		break;
	case 50:
		sprintf(errmsg,"RCV_BUFFER_OVERFLOW_ERROR");
		break;
	case 51:
		sprintf(errmsg,"RF_ERROR");
		break;
	case 52:
		sprintf(errmsg,"PROTOCOL_ERROR");
		break;
	case 53:
		sprintf(errmsg,"USER_BUFFER_FULL_ERROR");
		break;
	case 54:
		sprintf(errmsg,"BUADRATE_NOT_SUPPORTED");
		break;
	case 55:
		sprintf(errmsg,"INVAILD_FORMAT_ERROR");
		break;
	case 56:
		sprintf(errmsg,"LRC_ERROR");
		break;
	case 57:
		sprintf(errmsg,"FRAMERR");
		break;
	case 60:
		sprintf(errmsg,"WRONG_PARAMETER_VALUE");
		break;
	case 61:
		sprintf(errmsg,"INVAILD_PARAMETER_ERROR");
		break;
	case 62:
		sprintf(errmsg,"UNSUPPORTED_PARAMETER");
		break;
	case 63:
		sprintf(errmsg,"UNSUPPORTED_COMMAND");
		break;
	case 64:
		sprintf(errmsg,"INTERFACE_NOT_ENABLED");
		break;
	case 65:
		sprintf(errmsg,"ACK_SUPPOSED");
		break;
	case 66:
		sprintf(errmsg,"NACK_RECEVIED");
		break;
	case 67:
		sprintf(errmsg,"BLOCKNR_NOT_EQUAL");
		break;
	case 68:
		sprintf(errmsg,"TARGET_SET_TOX");
		break;
	case 69:
		sprintf(errmsg,"TARGET_RESET_TOX");
		break;
	case 70:
		sprintf(errmsg,"TARGET_DESELECTED");
		break;
	case 71:
		sprintf(errmsg,"TARGET_RELEASED");
		break;
	case 72:
		sprintf(errmsg,"ID_ALREADY_IN_USE");
		break;
	case 73:
		sprintf(errmsg,"INSTANCE_ALREADY_IN_USE");
		break;
	case 74:
		sprintf(errmsg,"ID_NOT_IN_USE");
		break;
	case 75:
		sprintf(errmsg,"NO_ID_AVAILABLE");
		break;
	case 76:
		sprintf(errmsg,"OTHER_ERROR");
		break;
	case 77:
		sprintf(errmsg,"INVALID_READER_STATE");
		break;
	case 78:
		sprintf(errmsg,"JOINER_TEMP_ERROR");
		break;	
		break;
	case 100:
		sprintf(errmsg,"NOTYET_IMPLEMENTED");
		break;
	case 109:
		sprintf(errmsg,"FIFO_ERROR");
		break;
	case 114:
		sprintf(errmsg,"WRONG_SELECT_COUNT");
		break;
	case 123:
		sprintf(errmsg,"WRONG_VALUE");
		break;
	case 124:
		sprintf(errmsg,"VALERR");
		break;
	case 126:
		sprintf(errmsg,"RE_INIT");
		break;
	case 127:
		sprintf(errmsg,"NO_INIT");
		break;
	case 144:
		sprintf(errmsg,"NFC_MODE_ERROR");
		break;
	case 145:
		sprintf(errmsg,"NFC_SPEED_ERROR");
		break;
	case 0xA0:
		sprintf(errmsg,"MAC_ACTIVE_ERR");
		break;
	case 0xA1:
		sprintf(errmsg,"LLC_CONNECT_ERR");
		break;
	case 0xA2:
		sprintf(errmsg,"LLC_SYMM_ERR");
		break;
	case 0xA3:
		sprintf(errmsg,"LLC_INF_ERR");
		break;
	case 0xA4:
		sprintf(errmsg,"LLC_RR_ERR");
		break;
	case 0xA5:
		sprintf(errmsg,"LLC_DISC_ERR");
		break;
	case 0xA6:
		sprintf(errmsg,"MAC_DEACTIVE_ERR");
		break;
	case 0xA7:
		sprintf(errmsg,"NDEF_ERR");
		break;
	case 0xA8:
		sprintf(errmsg,"CHECKSUM_ERR");
		break;
	case 0xA9:
		sprintf(errmsg,"LLC_GET_ERR");
		break;
	case 0xAA:
		sprintf(errmsg,"LLC_SET_ERR");
		break;
	case 0xAB:
		sprintf(errmsg,"LLC_MAGIC_ERR");
		break;
	case 0xAC:
		sprintf(errmsg,"LLC_LEN_ERR");
		break;
	case 0xAD:
		sprintf(errmsg,"LLC_AGF_ERR");
		break;
	case 0xAE:
		sprintf(errmsg,"SNEP_ERR");
		break;
	case 0xAF:
		sprintf(errmsg,"LLC_CLOSED");
		break;
	case 0xB0:
		sprintf(errmsg,"LLC_NOSERVICE");
		break;
	case 0xB1:
		sprintf(errmsg,"LLC_CON_REJECT");
		break;
	case 0xB2:
		sprintf(errmsg,"LLC_INVALID_PDU");
		break;
	case 0xB3:
		sprintf(errmsg,"LLC_INVALID_NR");
		break;
	case 0xB4:
		sprintf(errmsg,"LLC_INVALID_NS");
		break;
	case 0xB5:
		sprintf(errmsg,"LLC_SNLLEN_ERR");
		break;
	case 0xEF:
		sprintf(errmsg,"CARD_TYPE_ERR");
		break;
	case 1000:
		sprintf(errmsg,"INVALID_PORT");
		break;
	case 1001:
		sprintf(errmsg,"STX_ERROR");
		break;
	case 1002:
		sprintf(errmsg,"INVALID_LENGTH_ERROR");
		break;
	case 1003:
		sprintf(errmsg,"TIMEOUT_ERROR");
		break;
	case 1004:
		sprintf(errmsg,"CRC_ERROR");
		break;
	case 1005:
		sprintf(errmsg,"LRC_ERROR");
		break;
	case 1006:
		sprintf(errmsg,"RW_ERROR");
		break;
	case 1007:
		sprintf(errmsg,"ETX_ERROR");
		break;
	case 1008:
		sprintf(errmsg,"USB_WRITE_ERROR");
		break;
	case 1009:
		sprintf(errmsg,"USB_READ_ERROR");
		break;
	case 1010:
		sprintf(errmsg,"INVALID_SENDDATA_LEN");
		break;
	case 1011:
		sprintf(errmsg,"INVALID_SENDBUF_SIZE");
		break;
	case 1012:
		sprintf(errmsg,"TOO_SMALL_RECVBUF");
		break;
#if 0
	case APP_SENDBUF_OVERFLOW:
		sprintf(errmsg,"SENDBUF_OVERFLOW");
		break;
	case APP_FELICA_ERROR:
		sprintf(errmsg,((CDualCardDllApp*)AfxGetApp())->m_strFeliCaError;
		break;
	case APP_NFC_ERROR:
		sprintf(errmsg,((CDualCardDllApp*)AfxGetApp())->m_strNFCError;
		break;
	case APP_NET_ERROR:
		sprintf(errmsg,((CDualCardDllApp*)AfxGetApp())->m_strNetError;
		break;
#endif
	default:
		sprintf(errmsg,"UNKNOWN_ERROR(0x%02X)",errcode);
		break;
	}
int nLength = strlen(errmsg);
	memcpy(retmsg,errmsg,nLength);	
	retmsg[nLength] = 0;
	return nLength;
}



unsigned char LoByte(unsigned int zz)
{
	return (unsigned char)(zz&0x00ff);
}
unsigned char HiByte(unsigned int zz)
{
	return (unsigned char)((zz&0xff00)>>8);
}

libusb_context *ctx = NULL;

void USBCloseDevice( )
{
	if(dualiUsbDevice0 != NULL)	libusb_close(dualiUsbDevice0);
	if(dualiUsbDevice1 != NULL)	libusb_close(dualiUsbDevice1);
	libusb_exit(ctx);
}
//libusb_get_device_list
static int find_devs(libusb_device **devs,u16 vid,u16 pid)
{
	libusb_device *dev;
	int i = 0,j=0;
	struct libusb_device_handle *handle = NULL;

	while ((dev = devs[i++]) != NULL) {
		struct libusb_device_descriptor desc;
		int r = libusb_get_device_descriptor(dev, &desc);
		if (r < 0) {
			fprintf(stderr, "failed to get device descriptor");
			return 0;
		}
		if((desc.idVendor == vid)&&(desc.idProduct == pid)){
			printf("[dev%d]%04x:%04x (bus %d, device %d)\n",i,
				desc.idVendor, desc.idProduct,
				libusb_get_bus_number(dev), libusb_get_device_address(dev));

			r = libusb_open(dev, &handle);
			if (r < 0)
				handle = NULL;
			if(j==0) dualiUsbDevice0 = handle;
			else dualiUsbDevice1 = handle;
			j++;
		}
	}
	return j;
}

int USBOpenDevice(u16 pid)
{
	libusb_device **devs;
	ssize_t cnt,dp680cnt;
	libusb_device_handle *handle;
	libusb_init(&ctx);

	cnt = libusb_get_device_list(NULL, &devs);
	if (cnt < 0)
		return NULL;
	dp680cnt = find_devs(devs,USB_VENDOR_ID, pid);
	libusb_free_device_list(devs, 1);


	handle = libusb_open_device_with_vid_pid(ctx,USB_VENDOR_ID, pid);
	if(handle == NULL) return (void *)handle;
	libusb_device  *devsx;
	devsx = libusb_get_device(handle);
//	int cnt;
	inendpoint = 0;
	cnt = libusb_get_max_packet_size(devsx,1);
	if(cnt == 64) outendpoint = 1;
	else {
		cnt = libusb_get_max_packet_size(devsx,2);
		if(cnt == 64) outendpoint = 2;
	}
	inendpoint = (outendpoint | 0x80);
	printf("ep_in_endpoint %02X,ep_out_endpoint %02X\n",inendpoint,outendpoint);
	if (!handle) {
		perror("device not found");
	}
   	if(libusb_kernel_driver_active(handle, 0))   libusb_detach_kernel_driver(handle, 0); 

	return dp680cnt;
}

int USBDeviceDataExchange(int virtualPort, ioctl_arg *duali_protocol ,int timeout)
{
	libusb_device_handle *dualiUsbDevice;
	u8 sendbuf[4096];
	u8 recvbuf[4096];
	int sendlen;
	int recvlen;
	int nReturn;
	sendlen = 0;
	if(virtualPort == USBFS1) dualiUsbDevice = dualiUsbDevice0;
	else dualiUsbDevice = dualiUsbDevice1;

	sendbuf[sendlen++] = (duali_protocol->wlen+1)/256;
	sendbuf[sendlen++] = (duali_protocol->wlen+1)%256;
	sendbuf[sendlen++] = duali_protocol->cmd;


	if (duali_protocol->wlen >= 1)
		memcpy(sendbuf+sendlen, duali_protocol->buf, duali_protocol->wlen);

	sendlen += duali_protocol->wlen;

	if( libusb_claim_interface( dualiUsbDevice, 0 ) < 0 )
	{
		LOGE("usb claim interface error \n");
		return -1;			
	}
	nReturn = libusb_bulk_transfer(dualiUsbDevice, outendpoint, sendbuf, sendlen,	&sendlen, timeout);

	if( nReturn != 0 )
	{
		libusb_release_interface( dualiUsbDevice, 0 );				
		LOGE("usb bulk write error code: %d", nReturn);
		return nReturn;
	}

	nReturn = libusb_bulk_transfer(dualiUsbDevice, inendpoint, recvbuf, sizeof(recvbuf),
			&recvlen, timeout);

	if( nReturn < 0 )
	{
		libusb_release_interface( dualiUsbDevice, 0 );				
		LOGE("usb bulk read error code: %d", nReturn);
		return nReturn;
	}
	duali_protocol->rlen = recvbuf[0];
	duali_protocol->rlen *= 256;
	duali_protocol->rlen += recvbuf[1];
	duali_protocol->rlen--;

	nReturn = duali_protocol->cmd = recvbuf[2];

	memcpy(duali_protocol->buf, recvbuf+3, duali_protocol->rlen);
	
	libusb_release_interface( dualiUsbDevice, 0 );				

    	return nReturn;
}

int dualiUSBOpenDevice(u16 pid)
{
	int dualiUsbDevice;
	dualiUsbDevice = USBOpenDevice(pid);
	if( dualiUsbDevice == 0 )	return -1;
	return dualiUsbDevice;	
}
int dualiUSBCloseDevice(void)
{
	USBCloseDevice();
	return 0;
}

int DeviceDataExchange( int virtualPort,ioctl_arg* arg,int timeout)
{
	
	return(USBDeviceDataExchange(virtualPort,arg, timeout));
}


int DE_Polling(int virtualPort, int datalen, unsigned char* data, int* outlen, unsigned char* lpRes, int xtimeout)
{
	int retPort=-1;
	ioctl_arg arg;

	if(xtimeout == 0) xtimeout = 4000;
	arg.wlen = datalen-1;
	arg.cmd = data[0];

	if(datalen > 1)
		memcpy(arg.buf, data+1, datalen-1);
	
	retPort = DeviceDataExchange(virtualPort,&arg,xtimeout);

	if(retPort < 0) return retPort;
	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}


/////////////////////////////////////////////////////////////////////////////////////////
//Type C
/////////////////////////////////////////////////////////////////////////////////////////
int DEC_Transparent(int virtualPort, int datalen, unsigned char* data, int* outlen,
		unsigned char* lpRes, unsigned char tout)
{
	int retPort=-1;
	ioctl_arg arg;

	arg.cmd = _DEC_TRANSPARENT;
	arg.wlen = datalen + 2;
	arg.buf[0] = datalen + 1;
	memcpy(arg.buf+1, data, datalen);
	arg.buf[arg.wlen - 1] = tout;

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;
	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}

int DEC_Polling_NoENC(int virtualPort, unsigned char* systemcode, unsigned char requestsyscode,
		unsigned char timeslot, int* outlen, unsigned char* lpRes, unsigned char tout)
{
	int retPort=-1;
	ioctl_arg arg;

	arg.cmd = _DEC_POLLING_NOENC;
	arg.wlen = 5;
	arg.buf[0] = systemcode[0];
	arg.buf[1] = systemcode[1];
	arg.buf[2] = requestsyscode;
	arg.buf[3] = timeslot;
	arg.buf[4] = tout;

	retPort = DeviceDataExchange(virtualPort,&arg,3000);

	if(retPort < 0) return retPort;
	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}

int DEC_Read_NoENC(int virtualPort, unsigned char* IDm, unsigned char* servicecode,
		unsigned char block, int* outlen, unsigned char* lpRes, unsigned char tout)
{
	int retPort=-1;
	ioctl_arg arg;

	arg.cmd = _DEC_READ_NOENC;
	arg.wlen = 12;
	memcpy(arg.buf, IDm, 8);
	memcpy(arg.buf + 8, servicecode, 2);
	arg.buf[10] = block;
	arg.buf[11] = tout;

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}

int DEC_Write_NoENC(int virtualPort, unsigned char* IDm, unsigned char* servicecode,
		unsigned char block, unsigned char* blockdata, int* outlen, unsigned char* lpRes, unsigned char tout)
{
	int retPort=-1;
	ioctl_arg arg;
	int idx = 0;

	arg.cmd = _DEC_WRITE_NOENC;
	arg.wlen = 12;
	memcpy(arg.buf + idx, IDm, 8);
	idx += 8;
	memcpy(arg.buf + idx, servicecode, 2);
	idx += 2;
	arg.buf[idx++] = block;
	memcpy(arg.buf + idx, blockdata, 16);
	idx += 16;
	arg.buf[idx++] = tout;

	arg.wlen = idx;

	retPort = DeviceDataExchange(virtualPort,&arg,3000);

	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}
/////////////////////////////////////////////////////////////////////////////////////////
//Device control command
/////////////////////////////////////////////////////////////////////////////////////////
int DE_RFOn(int virtualPort, int* outlen, unsigned char* lpRes)
{
	int retPort=-1;
	ioctl_arg arg;

	arg.cmd = _DE_RFON;
	arg.wlen = 0;

	retPort = DeviceDataExchange(virtualPort,&arg,3000);

	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}

int DE_RFOff(int virtualPort, int* outlen, unsigned char* lpRes)
{
	int retPort=-1;
	ioctl_arg arg;

	arg.cmd = _DE_RFOFF;
	arg.wlen = 0;

	retPort = DeviceDataExchange(virtualPort,&arg,3000);

	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}

int DE_RFReset(int virtualPort, int* outlen, unsigned char* lpRes)
{
	int retPort=-1;
	ioctl_arg arg;

	arg.cmd = _DE_RESET;
	arg.wlen = 0;

	retPort = DeviceDataExchange(virtualPort,&arg,4000);

	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}


int DE_GetVersion(int virtualPort, int* outlen, unsigned char* lpRes)
{
	int retPort=-1;
	ioctl_arg arg;

	arg.cmd = _DE_VERSION;
	arg.wlen = 0;

	retPort = DeviceDataExchange(virtualPort,&arg,3000);

	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}

/////////////////////////////////////////////////////////////////////////////////////////
//Type A
/////////////////////////////////////////////////////////////////////////////////////////

int DEA_Reset(int virtualPort, unsigned char* lpdelay)
{
	int retPort=-1;
	ioctl_arg arg;

	arg.cmd = _DEA_RESET;
	arg.wlen = 2;
	memcpy(arg.buf, lpdelay, 2);

	retPort = DeviceDataExchange(virtualPort,&arg,3000);

	if(retPort < 0) return retPort;

	retPort = arg.cmd;


	return retPort;
}

int DEA_Idle_Req(int virtualPort, int* outlen, unsigned char* lpRes)
{
	int retPort=-1;
	ioctl_arg arg;

	arg.cmd = _DEA_IDLE_REQ;
	arg.wlen = 0;

	LOGD("[cmd: %02x]", arg.cmd);

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}

int DEA_Wakeup_Req(int virtualPort, int* outlen, unsigned char* lpRes)
{
	int retPort=-1;
	ioctl_arg arg;

	arg.cmd = _DEA_WAKEUP_REQ;
	arg.wlen = 0;

	retPort = DeviceDataExchange(virtualPort,&arg,3000);

	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}

int DEA_Anticoll(int virtualPort, int* outlen, unsigned char* lpRes)
{
	int retPort=-1;
	ioctl_arg arg;

	arg.cmd = _DEA_ANTICOLL;
	arg.wlen = 1;
	arg.buf[0] = 0x00;

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}

int DEA_Select(int virtualPort, unsigned char* uid, int* outlen, unsigned char* lpRes)
{
	int retPort=-1;
	ioctl_arg arg;

	arg.cmd = _DEA_SELECT;
	arg.wlen = 4;

	memcpy(arg.buf, uid, 4);

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}

int DEA_Auth(int virtualPort, unsigned char mode, unsigned char keyno, unsigned char blockno)
{
	int retPort=-1;
	ioctl_arg arg;

	arg.cmd = _DEA_AUTH;
	arg.wlen = 3;
	arg.buf[0] = mode;
	arg.buf[1] = keyno;
	arg.buf[2] = blockno;

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;


	return retPort;
}

int DEA_Halt(int virtualPort)
{
	int retPort=-1;
	ioctl_arg arg;

	arg.cmd = _DEA_HALT;
	arg.wlen = 0;

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;


	return retPort;
}

int DEA_Read(int virtualPort, unsigned char blockno, int* outlen, unsigned char* lpRes)
{
	int retPort=-1;
	ioctl_arg arg;

	arg.cmd = _DEA_READ;
	arg.wlen = 1;
	arg.buf[0] = blockno;

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}

int DEA_Write(int virtualPort, unsigned char blockno, int datalen, unsigned char* data)
{
	int retPort=-1;
	ioctl_arg arg;

	LOGD("[cmd: %02x, datalen:%02x]", arg.cmd, datalen);

	arg.cmd = _DEA_WRITE;
	arg.wlen = datalen + 1;
	arg.buf[0] = blockno;
	memcpy(arg.buf+1, data, 16);

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;


	return retPort;
}

int DEA_Increment(int virtualPort, unsigned char blockno, unsigned char* value)
{
	int retPort=-1;
	ioctl_arg arg;

	arg.cmd = _DEA_INCREMENT;
	arg.wlen = 5;
	arg.buf[0] = blockno;
	memcpy(arg.buf+1, value, 4);

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;


	return retPort;
}

int DEA_Decrement(int virtualPort, unsigned char blockno, unsigned char* value)
{
	int retPort=-1;
	ioctl_arg arg;

	arg.cmd = _DEA_DECREMENT;
	arg.wlen = 5;
	arg.buf[0] = blockno;
	memcpy(arg.buf+1, value, 4);

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;


	return retPort;
}

int DEA_Inc_Transfer(int virtualPort, unsigned char blockno, unsigned char* value, unsigned char trblockno)
{
	int retPort=-1;
	ioctl_arg arg;

	arg.cmd = _DEA_INC_TRANS;
	arg.wlen = 6;
	arg.buf[0] = blockno;
	memcpy(arg.buf+1, value, 4);
	arg.buf[5] = trblockno;

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;


	return retPort;
}

int DEA_Dec_Transfer(int virtualPort, unsigned char blockno, unsigned char* value, unsigned char trblockno)
{
	int retPort=-1;
	ioctl_arg arg;

	arg.cmd = _DEA_DEC_TRANS;
	arg.wlen = 6;
	arg.buf[0] = blockno;
	memcpy(arg.buf+1, value, 4);
	arg.buf[5] = trblockno;

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;


	return retPort;
}

int DEA_Restore(int virtualPort, unsigned char blockno)
{
	int retPort=-1;
	ioctl_arg arg;

	arg.cmd = _DEA_RESTORE;
	arg.wlen = 1;
	arg.buf[0] = blockno;

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;


	return retPort;
}

int DEA_Transfer(int virtualPort, unsigned char blockno)
{
	int retPort=-1;
	ioctl_arg arg;

	arg.cmd = _DEA_TRANSFER;
	arg.wlen = 1;
	arg.buf[0] = blockno;

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;


	return retPort;
}

int DEA_Loadkey(int virtualPort, unsigned char mode, unsigned char keyno, unsigned char* keydata)
{
	int retPort=-1;
	ioctl_arg arg;

	arg.cmd = _DEA_LOADKEY;
	arg.wlen = 8;
	arg.buf[0] = mode;
	arg.buf[1] = keyno;
	memcpy(arg.buf+2, keydata, 6);

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;


	return retPort;
}

int DEA_Authkey(int virtualPort, unsigned char mode, unsigned char* keydata, unsigned char blockno)
{
	int retPort=-1;
	ioctl_arg arg;

	arg.cmd = _DEA_AUTHKEY;
	arg.wlen = 8;
	arg.buf[0] = mode;
	memcpy(arg.buf+1, keydata, 6);
	arg.buf[7] = blockno;

	LOGD("%s, %d[cmd: %02x, mode:%02x, blockno:%02x]", __FUNCTION__, __LINE__, arg.cmd, mode, blockno);

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;


	return retPort;
}

int DEA_Req_Auth(int virtualPort, unsigned char requestmode, unsigned char authmode,
		unsigned char keyno, unsigned char blockno, int* outlen, unsigned char* lpRes)
{
	int retPort=-1;
	ioctl_arg arg;

	arg.cmd = _DEA_REQ_ANTI_AUTH;
	arg.wlen = 4;
	arg.buf[0] = requestmode;
	arg.buf[1] = authmode;
	arg.buf[2] = keyno;
	arg.buf[3] = blockno;

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}

int DEA_Req_Authkey(int virtualPort, unsigned char requestmode, unsigned char authmode,
		unsigned char blockno, unsigned char* keydata, int* outlen, unsigned char* lpRes)
{
	int retPort=-1;
	ioctl_arg arg;

	arg.cmd = _DEA_REQ_ANTI_AUTHKEY;
	arg.wlen = 9;
	arg.buf[0] = requestmode;
	arg.buf[1] = authmode;
	arg.buf[2] = blockno;
	memcpy(arg.buf+3, keydata, 6);

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}

int DEA_Inc_Transfer2(int virtualPort, unsigned char blockno, unsigned char* value, unsigned char trblockno)
{
	int retPort=-1;
	ioctl_arg arg;

	arg.cmd = _DEA_INC_TRANS2;
	arg.wlen = 6;
	arg.buf[0] = blockno;
	memcpy(arg.buf+1, value, 4);
	arg.buf[5] = trblockno;

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;


	return retPort;
}

int DEA_Dec_Transfer2(int virtualPort, unsigned char blockno, unsigned char* value, unsigned char trblockno)
{
	int retPort=-1;
	ioctl_arg arg;

	arg.cmd = _DEA_DEC_TRANS2;
	arg.wlen = 6;
	arg.buf[0] = blockno;
	memcpy(arg.buf+1, value, 4);
	arg.buf[5] = trblockno;

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;


	return retPort;
}

int DEA_Req_AuthRead(int virtualPort, unsigned char requestmode, unsigned char authmode,
		unsigned char keyno, unsigned char blockno, int* outlen, unsigned char* lpRes)
{
	int retPort=-1;
	ioctl_arg arg;

	arg.cmd = _DEA_REQ_ANTI_AUTH_RD;
	arg.wlen = 4;
	arg.buf[0] = requestmode;
	arg.buf[1] = authmode;
	arg.buf[2] = keyno;
	arg.buf[3] = blockno;

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}

int DEA_Req_AuthkeyRead(int virtualPort, unsigned char requestmode, unsigned char authmode,
		unsigned char blockno, unsigned char* keydata, int* outlen, unsigned char* lpRes)
{
	int retPort=-1;
	ioctl_arg arg;

	arg.cmd = _DEA_REQ_ANTI_AUTHKEY_RD;
	arg.wlen = 9;
	arg.buf[0] = requestmode;
	arg.buf[1] = authmode;
	arg.buf[2] = blockno;
	memcpy(arg.buf+3, keydata, 6);

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}

int DEA_Req_AuthWrite(int virtualPort, unsigned char requestmode, unsigned char authmode,
		unsigned char keyno, unsigned char blockno,  unsigned char* data, int* outlen, unsigned char* lpRes)
{
	int retPort=-1;
	ioctl_arg arg;

	arg.cmd = _DEA_REQ_ANTI_AUTH_WR;
	arg.wlen = 20;
	arg.buf[0] = requestmode;
	arg.buf[1] = authmode;
	arg.buf[2] = keyno;
	arg.buf[3] = blockno;
	memcpy(arg.buf+4, data, 16);

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}

int DEA_Req_AuthkeyWrite(int virtualPort, unsigned char requestmode, unsigned char authmode,
		unsigned char blockno,  unsigned char* keydata, unsigned char* data, int* outlen, unsigned char* lpRes)
{
	int retPort=-1;
	ioctl_arg arg;

	arg.cmd = _DEA_REQ_ANTI_AUTHKEY_WR;
	arg.wlen = 25;
	arg.buf[0] = requestmode;
	arg.buf[1] = authmode;
	arg.buf[2] = blockno;
	memcpy(arg.buf+3, keydata, 6);
	memcpy(arg.buf+9, data, 16);

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}

int DEA_Req_Select(int virtualPort, unsigned char requestmode, int* outlen, unsigned char* lpRes)
{
	int retPort=-1;
	ioctl_arg arg;

	arg.cmd = _DEA_REQ_ANTI_SEL;
	arg.wlen = 1;
	arg.buf[0] = requestmode;

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}

int DEA_UltraM_Write(int virtualPort, unsigned char address, unsigned char* data)
{
	int retPort=-1;
	ioctl_arg arg;

	arg.cmd = _DEA_UWRITE;
	arg.wlen = 5;
	arg.buf[0] = address;
	memcpy(arg.buf+1, data, 4);

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;


	return retPort;
}

int DEA_AntiSelLevel(int virtualPort, int* outlen, unsigned char* lpRes)
{
	int retPort=-1;
	ioctl_arg arg;

	arg.cmd = _DEA_ANTI_SEL_LEVEL;
	arg.wlen = 0;

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}

int DEA_AnticollLevel(int virtualPort, unsigned char cmd, unsigned char bitcnt, unsigned char* uid, int* outlen, unsigned char* lpRes)
{
	int retPort=-1;
	ioctl_arg arg;

	arg.cmd = _DEA_ANTICOLL_LEVEL;
	arg.wlen = 2;
	arg.buf[0] = cmd;
	arg.buf[1] = bitcnt;
	memcpy(arg.buf+2, uid, 4);

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}

int DEA_SelectLevel(int virtualPort, unsigned char cmd, unsigned char* uid , int* outlen, unsigned char* lpRes)
{
	int retPort=-1;
	ioctl_arg arg;

	arg.cmd = _DEA_SELECT_LEVEL;
	arg.wlen = 5;
	arg.buf[0] = cmd;
	memcpy(arg.buf+1, uid, 4);

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}

int DEA_DeviceInfo(int virtualPort, int* outlen, unsigned char* lpRes)
{
	int retPort=-1;
	ioctl_arg arg;

	arg.cmd = _DEA_DEVINFO;
	arg.wlen = 0;

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}

int DEA_Transparent(int virtualPort, unsigned char datalen, unsigned char* data, unsigned char TOUT, int* outlen, unsigned char* lpRes)
{
	int retPort=-1;
	ioctl_arg arg;

	arg.cmd = _DEA_TRANSPARENT;
	arg.wlen = datalen + 1;
	memcpy(arg.buf, data, datalen);
	memcpy(arg.buf+datalen, &TOUT, 1);

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}

int DEA_TransparentCRC(int virtualPort, unsigned char datalen, unsigned char* data, unsigned char* crc,
		unsigned char TOUT, int* outlen, unsigned char* lpRes)
{
	int retPort=-1;
	ioctl_arg arg;

	arg.cmd = _DEA_TRANSPARENT2;
	arg.wlen = datalen + 3;
	memcpy(arg.buf, data, datalen);
	memcpy(arg.buf+datalen, crc, 2);
	memcpy(arg.buf+(datalen+2), &TOUT, 1);

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}

int DEA_BitMode(int virtualPort, unsigned char datalen, unsigned char TxByteNo, unsigned char TxBitNo,
		unsigned char* data, int* outlen, unsigned char* lpRes)
{
	int retPort=-1;
	ioctl_arg arg;

	arg.cmd = _DEA_BITMODE;
	arg.wlen = datalen + 2;
	arg.buf[0] = TxByteNo;
	arg.buf[1] = TxBitNo;
	memcpy(arg.buf+2, data, datalen);

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}

int DEA_BitModeAnti(int virtualPort, unsigned char datalen, unsigned char TxByteNo, unsigned char TxBitNo,
		unsigned char* data, int* outlen, unsigned char* lpRes)
{
	int retPort=-1;
	ioctl_arg arg;

	arg.cmd = _DEA_BITMODEANTI;
	arg.wlen = datalen + 2;
	arg.buf[0] = TxByteNo;
	arg.buf[1] = TxBitNo;
	memcpy(arg.buf+2, data, datalen);

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}

int DEA_BitMode2(int virtualPort, unsigned char datalen, unsigned char TxByteNo, unsigned char TxBitNo,
		unsigned char* data, int* outlen, unsigned char* lpRes)
{
	int retPort=-1;
	ioctl_arg arg;

	arg.cmd = _DEA_BITMODE2;
	arg.wlen = datalen + 2;
	arg.buf[0] = TxByteNo;
	arg.buf[1] = TxBitNo;
	memcpy(arg.buf+2, data, datalen);

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}

/////////////////////////////////////////////////////////////////////////////////////////
//Type B
/////////////////////////////////////////////////////////////////////////////////////////

int DEB_Transparent(int virtualPort, unsigned char datalen, unsigned char* data, unsigned char TOUT, int* outlen, unsigned char* lpRes)
{
	int retPort=-1;
	ioctl_arg arg;

	arg.cmd = _DEB_TRANSPARENT;
	arg.wlen = datalen + 1;
	memcpy(arg.buf, data, datalen);
	memcpy(arg.buf+datalen, &TOUT, 1);

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}

int DEB_TransparentCRC(int virtualPort, unsigned char datalen, unsigned char* data, unsigned char* crc,
		unsigned char TOUT, int* outlen, unsigned char* lpRes)
{
	int retPort=-1;

	ioctl_arg arg;

	arg.cmd = _DEB_TRANSPARENT2;
	arg.wlen = datalen + 3;
	memcpy(arg.buf, data, datalen);
	memcpy(arg.buf+datalen, crc, 2);
	memcpy(arg.buf+(datalen+2), &TOUT, 1);

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}

int DEB_BFRAMING(int virtualPort, unsigned char Fvalue)
{
	int retPort=-1;

	ioctl_arg arg;

	arg.cmd = _DEB_BFRAMING;
	arg.wlen = 1;
	arg.buf[0] = Fvalue;

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;


	return retPort;
}

/////////////////////////////////////////////////////////////////////////////////////////
//TYPE A/B Common Function
/////////////////////////////////////////////////////////////////////////////////////////

int DE_APDU(int virtualPort, int datalen, unsigned char* data, int* outlen, unsigned char* lpRes)
{
	int retPort=-1;

	ioctl_arg arg;

	if ( data[0] == 0xFD )
	{
		arg.cmd = 0xFD;

		arg.wlen = data[4] + 4;	//CMD,P1,P2,Lc
		memcpy(arg.buf,data+1,arg.wlen+1);	

		LOGD("[DE_APDU, wlen:%d]", arg.wlen);
	}
	else 	if ( ( data[0] == 0xFE ) && (data[2] == 0xFE) && (data[3] == 0xFE) )
	{
		arg.cmd = data[1];

		if(data[4] == 0){
			if(datalen <= 6){
				arg.wlen = 0;
			}
			else{
				arg.wlen = data[5];
				arg.wlen *= 256;
				arg.wlen += data[6];

				memcpy(arg.buf,data+7,arg.wlen);
			}
		}
		else{
			arg.wlen = data[4];
			memcpy(arg.buf,data+5,arg.wlen);
		}
	}
	else
	{
		arg.cmd = _DE_APDU;
		arg.wlen = datalen;
		memcpy(arg.buf, data, datalen);
	}

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}


int DE_FindCard(int virtualPort, unsigned char baud, unsigned char cid,
		unsigned char nad, unsigned char option, int* outlen, unsigned char* lpRes)
{
	int retPort=-1;

	ioctl_arg arg;

	//LOGD("BAUD: %d", baud);

	arg.cmd = _DE_FIND_CARD;
	arg.wlen = 4;
	arg.buf[0] = baud;
	arg.buf[1] = cid;
	arg.buf[2] = nad;
	arg.buf[3] = option;

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}

int DE_FindTag (int virtualPort, int* outlen, unsigned char* lpRes)
{
	int retPort=-1;

	ioctl_arg arg;

	arg.wlen = 0;
	arg.cmd = _DE_PICC_CONNECT;

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}

int DEAB_RwWrite(int virtualPort, unsigned char Data)
{
	int retPort=-1;

	ioctl_arg arg;

	arg.cmd = _DEAB_RW_WRITE;
	arg.wlen = 1;
	arg.buf[0] = Data;

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;


	return retPort;
}

int DEAB_RwRead(int virtualPort, int* outlen, unsigned char* lpRes)
{
	int retPort=-1;

	ioctl_arg arg;

	arg.cmd = _DEAB_RW_READ;
	arg.wlen = 0;

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}


int DE_T1Bypass(int virtualPort, int apdulen, unsigned char slotno, unsigned char nad, unsigned char pcb, unsigned char lenth,
		unsigned char* apdu, unsigned char lrc, int* outlen, unsigned char* lpRes)
{
	int retPort=-1;

	ioctl_arg arg;

	arg.cmd = _DE_CARD_T1BYPASS;
	arg.wlen = apdulen+5;
	arg.buf[0] = slotno;
	arg.buf[1] = nad;
	arg.buf[2] = pcb;
	arg.buf[3] = lenth;
	memcpy(arg.buf+4, apdu, apdulen);
	arg.buf[apdulen+4] = lrc;

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}



int DE_CARD_APDU(int virtualPort, unsigned char slotno, int datalen, unsigned char* data, int* outlen, unsigned char* lpRes)
{
	int retPort=-1;

	ioctl_arg arg;

	arg.cmd = _DE_CARD_APDU;
	arg.wlen = datalen+1;
	arg.buf[0] = slotno;
	memcpy(arg.buf+1, data, datalen);

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}

int DE_CARD_PARITY_ERROR_TEST(int virtualPort, unsigned char Option1, unsigned char Option2, int* outlen, unsigned char* lpRes)
{
	int retPort=-1;

	ioctl_arg arg;

	arg.cmd = _DE_CARD_PARITY_ERROR_TEST;
	arg.wlen = 2;
	arg.buf[0] = Option1;
	arg.buf[1] = Option2;

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;

	return retPort;
}

//////////////////////////15693////////////////////////

int DED_Inventory(int virtualPort, unsigned char Flag, int* outlen, unsigned char* lpRes)
{
	int retPort=-1;

	ioctl_arg arg;

	arg.cmd = _DED_Inventory;
	arg.wlen = 1;
	arg.buf[0] = Flag;

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}

int DED_Select(int virtualPort, int* outlen, unsigned char* lpRes)
{
	int retPort=-1;

	ioctl_arg arg;

	arg.cmd = _DED_Select;
	arg.wlen = 0;

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}

int DED_Read(int virtualPort, unsigned char blockno, int* outlen, unsigned char* lpRes)
{
	int retPort=-1;

	ioctl_arg arg;

	arg.cmd = _DED_Read;
	arg.wlen = 1;
	arg.buf[0] = blockno;

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}

int DED_Write(int virtualPort, unsigned char blockno, unsigned char* data, int* outlen, unsigned char* lpRes)
{
	int retPort=-1;

	ioctl_arg arg;

	arg.cmd = _DED_Write;
	arg.wlen = 5;
	arg.buf[0] = blockno;
	memcpy(arg.buf+1, data, 4);

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}

int DED_Transparent(int virtualPort, int datalen, unsigned char* data, int* outlen, unsigned char* lpRes)
{
	int retPort=-1;

	ioctl_arg arg;

	arg.cmd = _DED_Transparent;
	arg.wlen = datalen;
	memcpy(arg.buf, data, datalen);

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}

int DED_Eof(int virtualPort, int* outlen, unsigned char* lpRes)
{
	int retPort=-1;

	ioctl_arg arg;

	arg.cmd = _DED_Eof;
	arg.wlen = 0;

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}

//////////////////////////NFC//////////////////////////
int DE_NFC_TAG_CMD(int virtualPort, unsigned char TagType,unsigned char TagCMD, int optdatalen, unsigned char* optdata, int* outlen, unsigned char* lpRes)
{
	int retPort=-1;

	ioctl_arg arg;

	arg.cmd = _NFC_TAG_COMMAND;
	arg.wlen = optdatalen + 2;
	arg.buf[0] = TagType;
	arg.buf[1] = TagCMD;
	memcpy(arg.buf+2, optdata, optdatalen);

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}

//////////////////////////Desfire//////////////////////
int DE_DESFIRE_BATCH_SaveValue(int virtualPort, unsigned char* aid, unsigned char* key, unsigned char keyNo,
		int keyValue, int* outlen, unsigned char* lpRes)
{
	int retPort=-1;

	ioctl_arg arg;

	arg.cmd = _DE_DESFIRE_BATCH;

	arg.buf[0] = 0x03;
	memcpy(arg.buf + 1, aid, 3);
	if(keyValue == 2) {
		arg.wlen = 21;
		memcpy(arg.buf + 4, key, 16);
		arg.buf[20] = keyNo;
	} else {
		arg.wlen = 29;
		memcpy(arg.buf + 4, key, 24);
		arg.buf[28] = keyNo;
	}
	LOGD("[cmd: %02x, rlen:%02x]", arg.cmd, arg.buf[20]);

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}

int DE_DESFIRE_BATCH_SaveValueAES(int virtualPort, unsigned char* aid, unsigned char* key, unsigned char keyNo,
		int* outlen, unsigned char* lpRes)
{
	int retPort=-1;

	ioctl_arg arg;

	arg.cmd = _DE_DESFIRE_BATCH;

	arg.buf[0] = 0x06;
	memcpy(arg.buf + 1, aid, 3);

	arg.wlen = 21;
	memcpy(arg.buf + 4, key, 16);
	arg.buf[20] = keyNo;

	LOGD("[cmd: %02x, rlen:%02x]", arg.cmd, arg.buf[20]);

	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}

int DE_DESFIRE_BATCH_Read(int virtualPort, unsigned char flag, unsigned char fileno, unsigned char* offset,
		unsigned char* filesize, int* outlen, unsigned char* lpRes)
{
	int retPort=-1;

	ioctl_arg arg;

	arg.cmd = _DE_DESFIRE_BATCH;
	arg.wlen = 10;

	arg.buf[0] = 0x04;
	arg.buf[1] = flag;
	arg.buf[2] = 0xBD;
	arg.buf[3] = fileno;

	memcpy(arg.buf + 4, offset, 3);
	memcpy(arg.buf + 7, filesize, 3);
	LOGD("[cmd: %02x, rlen:%02x]", arg.cmd, arg.buf[0]);
	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}

int DE_DESFIRE_BATCH_Write(int virtualPort, unsigned char flag, unsigned char fileno, unsigned char* offset,
		unsigned char* filesize, unsigned char* data, int dataLen, int* outlen, unsigned char* lpRes)
{
	int retPort=-1;

	ioctl_arg arg;

	arg.cmd = _DE_DESFIRE_BATCH;
	arg.wlen = 10 + dataLen;

	arg.buf[0] = 0x04;
	arg.buf[1] = flag;
	arg.buf[2] = 0x3D;
	arg.buf[3] = fileno;

	memcpy(arg.buf + 4, offset, 3);
	memcpy(arg.buf + 7, filesize, 3);
	memcpy(arg.buf + 10, data, dataLen);
	LOGD("[cmd: %02x, rlen:%02x]", arg.cmd, arg.buf[0]);
	retPort = DeviceDataExchange(virtualPort,&arg,3000);
	if(retPort < 0) return retPort;

	retPort = arg.cmd;
	if(arg.rlen > 0) memcpy(lpRes,arg.buf, arg.rlen);
	*outlen = arg.rlen;
	return retPort;
}



