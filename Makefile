#CROSS_COMPILER = /opt/cross-arm/arm-linux-
CC=$(CROSS_COMPILER)gcc

INCLUDE_LIBUSB = 1

OBJS =  dualcard_main.o DualDlleMbeddedUsb.o	
CFLAGS=-Wall -fno-strict-aliasing 

ifeq ($(INCLUDE_LIBUSB), 1)
  CFLAGS += -I./libusb 
  LDFLAGS=-lpthread -lusb -lusb-1.0	
else
  CFLAGS+=-I./libusb 
  LDFLAGS=-lpthread 	
  OBJS +=	libusb/core.o 	libusb/descriptor.o 	libusb/io.o libusb/sync.o libusb/linux_usbfs.o 
endif

TARGET=dualcard-linux


.SUFFIXES : .c .o
 
all : $(TARGET)
 
$(TARGET): $(OBJS)
	   $(CC) -o $@ $(OBJS) $(LDFLAGS)

clean :
	rm -f $(OBJS) $(TARGET) *~

