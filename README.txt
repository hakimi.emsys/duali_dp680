Directory Tree
.
├── DualDlleMbeddedUsb.c	: usb open,close,data exchange,de_polling implemetation source
├── Makefile			: makefile for gcc linux x86 or amd64
├── README.txt			: this file
├── dlldefine.h			: head files
├── dualcard_main.c		: example source codes terminal_version,de_findcard with de_polling functions
└── libusb			;libusb folder
    ├── config.h
    ├── core.c
    ├── descriptor.c
    ├── io.c
    ├── libusb.h
    ├── libusbi.h
    ├── linux_usbfs.c
    ├── linux_usbfs.h
    └── sync.c

1. Make File change option

	if you use libusb.so, Makefile line 4 change INCLUDE_LIBUSB = 0.
	else INCLUDE_LIBUSB = 1.
	if cross_compile is needed, Makefile line 1 change "CROSS_COMPILER = /opt/cross-arm/arm-linux-"

compile :
	make clean
	make

excute  : pc usb-dev is restricted with root-permission.
	You must be excuted under super-user conditions.
	sudo ./dualcard-linux

find duali usb device's pid
	$lsusb
	Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
	Bus 002 Device 004: ID 1db2:0635  
	Bus 002 Device 003: ID 0e0f:0002 VMware, Inc. Virtual USB Hub
	Bus 002 Device 002: ID 0e0f:0003 VMware, Inc. Virtual Mouse
	Bus 002 Device 001: ID 1d6b:0001 Linux Foundation 1.1 root hub

	---> duali usb's vid is 1db2
             ---> 0635 is pid

	in dualcard_main.c
	LINE 14 usbDevice = dualiUSBOpenDevice(0x0635); //PID=0635, 


